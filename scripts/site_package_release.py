

import sys
import os
import argparse
import shutil
import re
import zipfile


#--- defaults
packageDef = "platform"
manifest = ("application.js", "config", "db", "bin", "package.json", "public", "routes", "views")


#--- user args
parser = argparse.ArgumentParser()

parser.add_argument("-pname", type=str, default=packageDef, help="package-name. Default: " + packageDef )
parser.add_argument("-ploc", type=str, help="package source location")
parser.add_argument("-zip_out", type=str, help="path of ouput <release-zip> file")


args = parser.parse_args()

#--- did we get required arguments
if ((not args.ploc) and (not args.zip_out)) :
	sys.exit("either -ploc or -zip_out flags are mandatory") 



#--- are we in the right location?

if (not os.path.isfile("application.js") or not os.path.isdir("assets") or not os.path.isdir("views")):
	sys.exit("you should be in site root directory") 



#---------------------------------------------------
def copyPackage():
	#print("at copyPackage")

	#--- does the source location exist?
	if (not os.path.isdir(args.ploc)):
		sys.exit(args.ploc + " doesn't exist")

	print("Note: Assuming you already ran 'ng build --prod' in " + args.ploc);

	packageSrc = os.path.join(args.ploc, "dist")
	if (not os.path.isdir(packageSrc)):
		sys.exit(packageSrc + " doesn't exist")

	#--- delete (if exists) and make new destination directory 
	packageDst = os.path.join("public", args.pname)
	if (os.path.isdir(packageDst)):
		shutil.rmtree(packageDst)
	os.makedirs(packageDst)
	

	#--- copy the dist
	for filename in os.listdir(packageSrc):
		if (filename == "index.html"):
			continue
		if (filename == "favicon.ico"):
			continue
		# print(filename)
		shutil.copyfile(os.path.join(packageSrc, filename), os.path.join(packageDst, filename))


	#--- handle the index.html --> package.hbs
	ifile = open(os.path.join(packageSrc, "index.html"), "r")
	ofile = open(os.path.join("views", (args.pname + ".hbs")), "w")

	for line in ifile:
		if ('src="inline.js"' in line):
			line = re.sub('src=\"','src=\"./' + args.pname + "/", line)
		ofile.write(line)

	ifile.close()
	ofile.close()

	 
	#--- copy the images. Don't delete existing images that may belong to other packages!
	imagesSrc = os.path.join(args.ploc, "src", "images")
	if (not os.path.isdir(imagesSrc)):
		sys.exit(imagesSrc + " doesn't exist")

	imagesDst = os.path.join("public", "images")
	if (not os.path.isdir(imagesDst)):
		os.makedirs(imagesDst)

	for filename in os.listdir(imagesSrc):
		if (os.path.isfile(os.path.join(imagesSrc, filename))):
			shutil.copyfile(os.path.join(imagesSrc, filename), os.path.join(imagesDst, filename))
		if (os.path.isdir(os.path.join(imagesSrc, filename))):
			if (os.path.isdir(os.path.join(imagesDst, filename))):
				shutil.rmtree(os.path.join(imagesDst, filename))
			shutil.copytree(os.path.join(imagesSrc, filename), os.path.join(imagesDst, filename))

	#---- finalize
	print("Package copied succesfully");




#-----------------------------------------------------

try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), compress_type=compression)


def createZip():

	print("Creating zip")

	#--- check for full manifest 
	for m in manifest:
		if (not (os.path.isfile(m) or os.path.isdir(m))):
			sys.exit("Can't find " + m)

	zf = zipfile.ZipFile(args.zip_out, mode='w')
	try:
		for m in manifest:
			print('\tadding ' + m + "...")
			if (os.path.isdir(m)):
				zipdir(m, zf)
			else:
				zf.write(m, compress_type=compression)
	finally:
		zf.close()
		print("Zip file " + args.zip_out + " was created")


#-----------------------------------------------------


#--- main

if (args.ploc):
	copyPackage()

if (args.zip_out):
	createZip();













