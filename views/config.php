<?php

// reCaptcha Setup
// ===============

// Insert below your reCaptcha Site and Secret Keys
// Go to https://www.google.com/recaptcha/admin/create if you don't have the keys yet

$publickey = "6LevLRAUAAAAAJrFLb_1LLGtP9k3qBAbQoLPU85A"; // Site key
$privatekey = "6LevLRAUAAAAALnK0ytDZBD7yu7q54GFeagEo6iT"; // Secret key

// Mail Setup
// ==========

// Sender Name and <email address> separated by space
$mail_sender = 'Support <contact@waycare-smart-highway.com>';
// Your Email Address where new emails will be sent to
$to_email = 'contact@waycare-smart-highway.com';
// Email Subject
$mail_subject = 'Support Request';

// Email content can be modified in the sendmail.php file.

?>