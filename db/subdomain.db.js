/**
 * Created by idanhahn on 12/11/2016.
 */

var https = require("https")


exports.getAll = function(subdomain_url, cb){
    var req = https.request('https://subdomain-test.firebaseio.com/subdomains/' + subdomain_url + '.json', function(res){
        var output = '';
        res.setEncoding('utf8');
        
        res.on('data', function (chunk) {
            console.log('in data');
            output += chunk;
        });
        res.on('end', function() {
            
            console.log('in end ' + output);
            
            var obj = JSON.parse(output);
            cb(res.statusCode, obj);
        });
    })
    req.on('error', function(err) {
        //res.send('error: ' + err.message);
    });

    req.end();
}

/*
Options:
subdomain
 */

exports.getSubDomain = function(options, cb){
    
}


