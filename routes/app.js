var express = require('express');
var router = express.Router();

var subdomains = require('../db/subdomain.db');

var url = 'https://subdomain-test.firebaseio.com/subdomains';

var options = {
    host: 'subdomain-test.firebaseio.com/subdomains',
    port: 443,
    path: 'index1.json',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};


// send email
var mailer = require('../public/assets/js/mailer');
router.post('/send_mail', function (req, res) {

    //console.log("in send_mail: req.body", req.body);

    if (!req.body['message']) {
        res.status(400).send('Can not send without message!')
        return;
    }

    if (!req.body['email']) {
        res.status(400).send('Can not send without email!')
        return;
    }

    if (!req.body['name']) {
        res.status(400).send('Can not send without name!')
        return;
    }


    mailer.sendEmail(req.body['message'], req.body['email'], req.body['name']);

    var response = {
        status: 200,
        success: 'Email sent successfully'
    }
    res.end(JSON.stringify(response));

});


/* GET home page. */
router.get('/*', function (req, res, next) {

    var subdomain_url = req.subdomains[req.subdomains.length - 1];
    console.log(subdomain_url);
    if (subdomain_url === 'www') {
        subdomain_url = req.subdomains[req.subdomains.length - 2];
        console.log(subdomain_url);
    }
    ;
    switch (subdomain_url) {
        case "lv-platform":
            res.render('lv-platform');
            break;
        case "ayalon":
        case "fll":
        case "lv":
        case "tampa":
            res.render('platform', {site: subdomain_url});
            break;

        case "old":
            res.render('old');
            break;

        case "new":
            res.render('new');
            break;

        default:
            res.render('new');
    };

});


module.exports = router;
