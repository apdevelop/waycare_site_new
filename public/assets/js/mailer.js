var nodemailer = require('nodemailer');

// Configuration
var fromAddr = "waycare.tech.webserver@gmail.com";  // dedicated gmail account
var password = "way-tech-serv1";

//var toAddr = "avi1p1@gmail.com";  - for testing only...
var toAddr = "contact@waycare-smart-highway.com"; // multiple addresses seperated by comma

exports.sendEmail = function (message, fromEmail, fromName) {

    //console.log('sendMyEmail:', message, fromEmail, fromName);

    var smtpConfig = {
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: fromAddr,
            pass: password
        }
    };

    var subject = fromName + " contacts Waycare team";

    var full_message = "User: " + fromName + " <" + fromEmail + ">\n\n" + message;


    var transporter = nodemailer.createTransport(smtpConfig);


    // setup e-mail data with unicode symbols
    var mailOptions = {
        to: toAddr, // list of receivers
        subject: subject, // Subject line
        text: full_message, // plaintext body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });



}
