angular.module('CarouselCtrl',[]).controller('CarouselCtrl', function ($scope) {
  
  $scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  var slides = $scope.slides = [];
  var currIndex = 0;

  // load images:
  var img1 = {
  	image: 'img/goal/tmc.jpg',
  	text: 'T.M.C',
  	id: 0
  }
  var img2 = {
  	image: 'img/goal/curSol1.jpg',
  	text: 'Current solution 1',
  	id: 1
  }
  var img3 = {
  	image: 'img/goal/curSol2.jpg',
  	text: 'Current solution 2',
  	id: 2
  }
  var img4 = {
  	image: 'img/goal/curSol3.jpg',
  	text: 'Current solution 3',
  	id: 3
  }
  var img5 = {
  	image: 'img/goal/avoid.jpg',
  	text: 'Avoid',
  	id: 4
  }




  slides.push(img1);
  slides.push(img2);
  slides.push(img3);
  slides.push(img4);
  slides.push(img5);

});