import {Component, Input, OnInit, ElementRef} from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent{

    data: string;
    
    constructor(public elementRef: ElementRef) {
        var native = this.elementRef.nativeElement;
        this.data = native.getAttribute("data");
        console.log("in c")
        console.log(this.data)
    }
    
    
}